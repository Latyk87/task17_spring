package com.epam;

import com.epam.view.View;

/**
 * This class is the enter point in  com.epam.Application.
 *
 * @version 2.1
 * Created by Borys Latyk on 13/11/2019.
 * @since 10.11.2019
 */
public class Application {
    /**
     * It is the main method of com.epam.Application.
     */
    public static void main(String[] args) {

        try {
            new View().show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
