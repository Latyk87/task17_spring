package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * POJO class BeanD.
 *
 * @version 2.1
 * Created by Borys Latyk on 30/12/2019.
 * @since 30.12.2019
 */
public class BeanD implements BeanValidator {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private String named;
    private int valued;

    public BeanD(String named, int valued) {
        this.named = named;
        this.valued = valued;
        logger1.info("D");
    }

    public String getNamed() {
        return named;
    }

    public int getValued() {
        return valued;
    }

    public void setNamed(String named) {
        this.named = named;
    }

    public void setValued(int valued) {
        this.valued = valued;
    }


    @Override
    public void validate() {
        if(this.named!=null&&this.valued>0){
            logger1.info(this+" Validated");
        }
    }

    @Override
    public String toString() {
        return "BeanD{" +
                "name='" + named + '\'' +
                ", value=" + valued +
                '}';
    }
}
