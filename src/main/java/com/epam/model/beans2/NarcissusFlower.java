package com.epam.model.beans2;

import org.springframework.stereotype.Component;
/**
 * POJO class for test package scan.
 *
 * @version 2.1
 * Created by Borys Latyk on 31/12/2019.
 * @since 31.12.2019
 */
@Component
public class NarcissusFlower {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "NarcissusFlower{" +
                "name='" + name + '\'' +
                '}';
    }
}
