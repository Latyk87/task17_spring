package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * POJO class BeanC.
 *
 * @version 2.1
 * Created by Borys Latyk on 30/12/2019.
 * @since 30.12.2019
 */
public class BeanC implements BeanValidator {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private String namec;
    private int valuec;

    public BeanC(){

    }
    public BeanC(String namec, int valuec) {
        this.namec = namec;
        this.valuec = valuec;
        logger1.info("C");
    }

    public String getNamec() {
        return namec;
    }

    public int getValuec() {
        return valuec;
    }

    public void setNamec(String namec) {
        this.namec = namec;
    }

    public void setValuec(int valuec) {
        this.valuec = valuec;
    }

    @Override
    public void validate() {
        if(this.namec!=null&&this.valuec>0){
            logger1.info(this+" Validated");
        }
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + namec + '\'' +
                ", value=" + valuec +
                '}';
    }
}
