package com.epam.model;
/**
 * Interface for Beans validation.
 *
 * @version 2.1
 * Created by Borys Latyk on 30/12/2019.
 * @since 30.12.2019
 */
public interface BeanValidator {
    void validate();
}
