package com.epam.model;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
/**
 * Configuration class for package scan test.
 *
 * @version 2.1
 * Created by Borys Latyk on 31/12/2019.
 * @since 31.12.2019
 */
@Configuration
@ComponentScan(basePackages = {"com.epam.model.beans2","com.epam.model.beans3"}, useDefaultFilters = false,
        includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
        classes = com.epam.model.beans3.BeanD.class))
@ComponentScan(basePackages = {"com.epam.model.beans2","com.epam.model.beans3"}, useDefaultFilters = false,
        includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
                classes = com.epam.model.beans3.BeanF.class))
public class ConfigurationSecond {
}
