package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * POJO class BeanB.
 *
 * @version 2.1
 * Created by Borys Latyk on 30/12/2019.
 * @since 30.12.2019
 */
public class BeanB implements BeanValidator {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private String nameb;
    private int valueb;

    public BeanB() {

    }

    public BeanB(String nameb, int valueb) {
        this.nameb = nameb;
        this.valueb = valueb;
        logger1.info("B");
    }

    public String getNameb() {
        return nameb;
    }

    public int getValueb() {
        return valueb;
    }

    public void setNameb(String nameb) {
        this.nameb = nameb;
    }

    public void setValueb(int valueb) {
        this.valueb = valueb;
    }



    @Override
    public void validate() {
        if(this.nameb!=null&&this.valueb>0){
            logger1.info(this+" Validated");
        }
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + nameb + '\'' +
                ", value=" + valueb +
                '}';
    }
}
