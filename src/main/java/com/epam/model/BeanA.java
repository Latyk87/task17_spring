package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
/**
 * POJO class BeanA.
 *
 * @version 2.1
 * Created by Borys Latyk on 30/12/2019.
 * @since 30.12.2019
 */
public class BeanA implements BeanValidator, InitializingBean, DisposableBean {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private String name;
    private int value;

   public BeanA(BeanB beanB){
       this.name=beanB.getNameb();
       this.value=beanB.getValueb();

   }
    public BeanA(BeanC beanC){
        this.name=beanC.getNamec();
        this.value=beanC.getValuec();

    }
    public BeanA(BeanD beanD){
        this.name=beanD.getNamed();
        this.value=beanD.getValued();

    }
    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public void validate() {
    if(this.name!=null&&this.value>0){
        logger1.info(this+" Validated");
    }
    }

    @Override
    public void destroy() throws Exception {
        logger1.info("BeanA - destroyed");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger1.info("BeanA - configured");
    }

    @Override
    public String toString() {

       return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
