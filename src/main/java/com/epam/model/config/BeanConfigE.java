package com.epam.model.config;

import com.epam.model.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Configuration of BeanE.
 *
 * @version 2.1
 * Created by Borys Latyk on 30/12/2019.
 * @since 30.12.2019
 */
@Configuration
@Import({BeanConfigInjectingA.class, BeanConfigInjectingAC.class,
        BeanConfigInjectingAD.class})
public class BeanConfigE {
    @Bean
    public BeanE getBeanE(BeanB beanB, BeanC beanC, BeanD beanD) {
        return new BeanE(beanC);
    }

    @Bean
    public ValidationBean getValidationBean() {
        return new ValidationBean();
    }
}
