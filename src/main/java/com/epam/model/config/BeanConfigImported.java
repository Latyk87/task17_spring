package com.epam.model.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Imported configuration.
 *
 * @version 2.1
 * Created by Borys Latyk on 30/12/2019.
 * @since 30.12.2019
 */
@Configuration
@Import(BeanConfigFirst.class)
public class BeanConfigImported {
}
