package com.epam.model.config;


import com.epam.model.BeanF;

import com.epam.model.ValidationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * Lazy configuration of BeanF.
 *
 * @version 2.1
 * Created by Borys Latyk on 30/12/2019.
 * @since 30.12.2019
 */
@Configuration
public class BeanConfigF {
    @Bean
    @Lazy
    public BeanF getBeanE() {
        BeanF beanF = new BeanF();
        beanF.setName("Hawk");
        beanF.setValue(4);
        return beanF;
    }

    @Bean
    public ValidationBean getValidationBean() {
        return new ValidationBean();
    }
}
