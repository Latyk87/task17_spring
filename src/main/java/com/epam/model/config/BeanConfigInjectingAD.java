package com.epam.model.config;

import com.epam.model.BeanA;
import com.epam.model.BeanC;
import com.epam.model.BeanD;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
/**
 * Configuration BeanA with injection.
 *
 * @version 2.1
 * Created by Borys Latyk on 30/12/2019.
 * @since 30.12.2019
 */
@Configuration
@PropertySource("bean.properties")
public class BeanConfigInjectingAD {
    @Value("${BeanB.nameb}")
    private String nameb;
    @Value("${BeanB.valueb}")
    private int valueb;

    @Value("${BeanC.namec}")
    private String namec;
    @Value("${BeanC.valuec}")
    private int valuec;

    @Value("${BeanD.named}")
    private String named;
    @Value("${BeanD.valued}")
    private int valued;

    @Bean
    public BeanC getBeanC(){
        return new BeanC(namec,valuec);
    }

    @Bean
    public BeanD getBeanD(){
        return new BeanD(named,valued);
    }

    @Bean
    public BeanA getBeanA(BeanD getBeanD){

        return new BeanA(getBeanD);
    }
}
