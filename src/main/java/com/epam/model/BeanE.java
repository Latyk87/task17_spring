package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
/**
 * POJO class BeanE.
 *
 * @version 2.1
 * Created by Borys Latyk on 30/12/2019.
 * @since 30.12.2019
 */
public class BeanE implements BeanValidator {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private String name;
    private int value;

    public BeanE(BeanB beanB) {
        this.name = beanB.getNameb();
        this.value = beanB.getValueb();
    }
    public BeanE(BeanC beanC) {
        this.name = beanC.getNamec();
        this.value = beanC.getValuec();
    }
    public BeanE(BeanD beanD) {
        this.name = beanD.getNamed();
        this.value = beanD.getValued();
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public void validate() {
        if(this.name!=null&&this.value>0){
            logger1.info(this+" Validated");
        }
    }
    @PostConstruct
    public void init() {
        logger1.info("C-Initialized");
    }
    @PreDestroy
    public void destroy() {
        logger1.info("C-Destroyed");
    }
    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
