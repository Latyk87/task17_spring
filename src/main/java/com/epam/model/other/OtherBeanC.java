package com.epam.model.other;

import org.springframework.stereotype.Component;
/**
 * POJO class for injecting with autowired.
 *
 * @version 2.1
 * Created by Borys Latyk on 31/12/2019.
 * @since 31.12.2019
 */
@Component
public class OtherBeanC {
    @Override
    public String toString() {
        return "OtherBeanC{} "+hashCode();
    }
}
