package com.epam.model.other;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
 * POJO class for injecting with autowired.
 *
 * @version 2.1
 * Created by Borys Latyk on 31/12/2019.
 * @since 31.12.2019
 */
@Component
@Scope("singleton")
public class OtherBeanA {

    @Override
    public String toString() {
        return "OtherBeanA{} "+hashCode();
    }
}
