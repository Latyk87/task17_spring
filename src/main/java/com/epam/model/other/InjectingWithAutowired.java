package com.epam.model.other;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
/**
 * POJO class for injecting with autowired.
 *
 * @version 2.1
 * Created by Borys Latyk on 31/12/2019.
 * @since 31.12.2019
 */
@Component
public class InjectingWithAutowired {
private OtherBeanA otherBeanA;
private OtherBeanB otherBeanB;
@Autowired
private OtherBeanC otherBeanC;

@Autowired
    public InjectingWithAutowired(OtherBeanA otherBeanA) {
        this.otherBeanA = otherBeanA;

    }
    @Autowired
    public void setOtherBeanB(OtherBeanB otherBeanB) {
        this.otherBeanB = otherBeanB;
    }



    @Override
    public String toString() {
        return "InjectingWithAutowired{" +
                "otherBeanA=" + otherBeanA +
                ", otherBeanB=" + otherBeanB +
                ", otherBeanC=" + otherBeanC +
                '}';
    }
}
