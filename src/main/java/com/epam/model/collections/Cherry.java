package com.epam.model.collections;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
/**
 * POJO class for collection beans.
 *
 * @version 2.1
 * Created by Borys Latyk on 31/12/2019.
 * @since 31.12.2019
 */
@Component
@Order(2)
public class Cherry implements Fruits {
    @Override
    public String getColor() {
        return "red "+getClass().toString();
    }
}
