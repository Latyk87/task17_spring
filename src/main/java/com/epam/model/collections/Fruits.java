package com.epam.model.collections;
/**
 * Interface with fruits color.
 *
 * @version 2.1
 * Created by Borys Latyk on 31/12/2019.
 * @since 31.12.2019
 */
public interface Fruits {
String getColor();
}
