package com.epam.model.collections;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;
/**
 * POJO class for collection beans.
 *
 * @version 2.1
 * Created by Borys Latyk on 31/12/2019.
 * @since 31.12.2019
 */
@Component
public class FruitsStore {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    @Autowired
    private List<Fruits> fruits;

public void printFruits(){
    for (Fruits f:fruits) {
        logger1.info(f.getColor());
    }
}
}
