package com.epam.model;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
/**
 * Configuration class for  injecting with autowired.
 *
 * @version 2.1
 * Created by Borys Latyk on 31/12/2019.
 * @since 31.12.2019
 */
@Configuration
@ComponentScan("com.epam.model.other")
public class ConfigurationInjectingC {
}
