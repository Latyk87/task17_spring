package com.epam.view;

import com.epam.Application;
import com.epam.model.*;
import com.epam.model.collections.FruitsStore;
import com.epam.model.config.*;
import com.epam.model.other.InjectingWithAutowired;
import com.epam.model.other.OtherBeanA;
import com.epam.model.other.OtherBeanB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Class to connect with controller, this class just show the data.
 *
 * @version 2.1
 * Created by Borys Latyk on 13/11/2019.
 * @since 10.11.2019
 */
public class View {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    BufferedReader input = new BufferedReader
            (new InputStreamReader(System.in));


    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", ("1 - BeanB, BeanC, BeanD from properties file"));
        menu.put("2", ("2 - Imported configuration"));
        menu.put("3", ("3 - Injecting BeanB"));
        menu.put("4", ("4 - Injecting BeanС"));
        menu.put("5", ("5 - Injecting BeanD"));
        menu.put("6", ("6 - Ordered  creation of Beans"));
        menu.put("7", ("7 - BeanE"));
        menu.put("8", ("8 - BeanF"));
        menu.put("9", ("9 - All beans"));
        menu.put("10", ("10 - All Beans validation"));
        menu.put("11", ("11 - Scan beans1"));
        menu.put("12", ("12 - Scan beans2,beans3"));
        menu.put("13", ("13 - Injecting with Autowired"));
        menu.put("14", ("14 - Collection for beans"));
        menu.put("15", ("15 - Scope"));
        menu.put("Q", ("Q - Exit"));
    }

    public View() {
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);
        methodsMenu.put("8", this::pressButton8);
        methodsMenu.put("9", this::pressButton9);
        methodsMenu.put("10", this::pressButton10);
        methodsMenu.put("11", this::pressButton11);
        methodsMenu.put("12", this::pressButton12);
        methodsMenu.put("13", this::pressButton13);
        methodsMenu.put("14", this::pressButton14);
        methodsMenu.put("15", this::pressButton15);
        methodsMenu.put("Q", this::pressButton16);


    }

    private void pressButton1() {
        ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfigFirst.class);
        BeanB beanB = context.getBean(BeanB.class);
        BeanC beanC = context.getBean(BeanC.class);
        BeanD beanD = context.getBean(BeanD.class);
        logger1.info(beanB);
        logger1.info(beanC);
        logger1.info(beanD);
    }

    private void pressButton2() {
        ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfigImported.class);
        BeanB beanB = context.getBean(BeanB.class);
        BeanC beanC = context.getBean(BeanC.class);
        BeanD beanD = context.getBean(BeanD.class);
        logger1.info(beanB);
        logger1.info(beanC);
        logger1.info(beanD);
    }


    private void pressButton3() {
        ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfigInjectingA.class);
        BeanA beanA = context.getBean(BeanA.class);
        logger1.info(beanA);
    }

    private void pressButton4() {
        ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfigInjectingAC.class);
        BeanA beanA = context.getBean(BeanA.class);
        logger1.info(beanA);
    }

    private void pressButton5() {
        ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfigInjectingAD.class);
        BeanA beanA = context.getBean(BeanA.class);
        logger1.info(beanA);
    }

    private void pressButton6() {
        ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfigInizialization.class);

        BeanD beanD = context.getBean(BeanD.class);
        BeanB beanB = context.getBean(BeanB.class);
        BeanC beanC = context.getBean(BeanC.class);
        logger1.info(beanB);
        logger1.info(beanC);
        logger1.info(beanD);
    }

    private void pressButton7() {
        ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfigE.class);
        BeanE beanE = context.getBean(BeanE.class);
        logger1.info(beanE);
    }

    private void pressButton8() {
        ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfigF.class);
        BeanF beanF = context.getBean(BeanF.class);
        logger1.info(beanF);
    }

    private void pressButton9() {
        ApplicationContext context = new AnnotationConfigApplicationContext
                (BeanConfigBCD.class, BeanConfigE.class, BeanConfigF.class, BeanConfigInjectingAC.class);
        BeanA beanA = context.getBean(BeanA.class);
        BeanB beanB = context.getBean(BeanB.class);
        BeanC beanC = context.getBean(BeanC.class);
        BeanD beanD = context.getBean(BeanD.class);
        BeanF beanF = context.getBean(BeanF.class);
        logger1.info(beanA);
        logger1.info(beanB);
        logger1.info(beanC);
        logger1.info(beanD);
        logger1.info(beanF);
    }

    private void pressButton10() {
        new AnnotationConfigApplicationContext(BeanConfigBCD.class, BeanConfigE.class, BeanConfigF.class, BeanConfigInjectingAC.class);
    }

    private void pressButton11() {
        ApplicationContext context = new AnnotationConfigApplicationContext(ConfigurationFirst.class);
        for (String beanName : context.getBeanDefinitionNames()) {
            logger1.info(context.getBean(beanName).getClass().toString());
        }
    }

    private void pressButton12() {
        ApplicationContext context = new AnnotationConfigApplicationContext(ConfigurationSecond.class);
        for (String beanName:context.getBeanDefinitionNames()) {
            logger1.info(context.getBean(beanName).getClass().toString());
        }
    }

    private void pressButton13() {
        ApplicationContext context = new AnnotationConfigApplicationContext(ConfigurationInjectingC.class);
        logger1.info(context.getBean(InjectingWithAutowired.class));
    }

    private void pressButton14() {
        ApplicationContext context = new AnnotationConfigApplicationContext(ConfigurationFruits.class);
        context.getBean(FruitsStore.class).printFruits();
    }

    private void pressButton15() {
        ApplicationContext context = new AnnotationConfigApplicationContext(ConfigurationInjectingC.class);
        logger1.info(context.getBean(OtherBeanA.class));
        logger1.info(context.getBean(OtherBeanA.class));
        logger1.info(context.getBean(OtherBeanB.class));
        logger1.info(context.getBean(OtherBeanB.class));
    }

    private void pressButton16() {
        logger1.info("Bye-Bye");
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger1.info("\nSpring:");
        for (String str : menu.values()) {
            logger1.info(str);
        }
    }

    public void show() throws Exception {
        String keyMenu;
        do {
            outputMenu();
            logger1.info("Please, select menu point");
            keyMenu = input.readLine().toUpperCase();
            methodsMenu.get(keyMenu).print();
        } while (!keyMenu.equals("Q"));
    }
}


